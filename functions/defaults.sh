#!/bin/sh

## live-build(7) - System Build Scripts
## Copyright (C) 2016-2020 The Debian Live team
## Copyright (C) 2006-2015 Daniel Baumann <mail@daniel-baumann.ch>
##
## This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.


New_configuration ()
{
	## Runtime

	# Image: Architecture
	if [ $(which dpkg) ]
	then
		CURRENT_IMAGE_ARCHITECTURE="$(dpkg --print-architecture)"
	else
		case "$(uname -m)" in
			x86_64)
				CURRENT_IMAGE_ARCHITECTURE="amd64"
				;;

			i?86)
				CURRENT_IMAGE_ARCHITECTURE="i386"
				;;

			*)
				Echo_warning "Unable to determine current architecture, using ${CURRENT_IMAGE_ARCHITECTURE}"
				;;
		esac
	fi


	## Configuration

	# Configuration-Version
	LIVE_CONFIGURATION_VERSION="${LIVE_CONFIGURATION_VERSION:-$(Get_configuration config/build Configuration-Version)}"
	LIVE_CONFIGURATION_VERSION="${LIVE_CONFIGURATION_VERSION:-${LIVE_BUILD_VERSION}}"
	export LIVE_CONFIGURATION_VERSION

	# Image: Name
	LIVE_IMAGE_NAME="${LIVE_IMAGE_NAME:-$(Get_configuration config/build Name)}"
	LIVE_IMAGE_NAME="${LIVE_IMAGE_NAME:-live-image}"
	export LIVE_IMAGE_NAME

	# Image: Architecture (FIXME: Support and default to 'any')
	LB_ARCHITECTURES="${LB_ARCHITECTURES:-$(Get_configuration config/build Architecture)}"
	LB_ARCHITECTURES="${LB_ARCHITECTURES:-${CURRENT_IMAGE_ARCHITECTURE}}"
	export LB_ARCHITECTURES

	# Image: Archive Areas
	LB_ARCHIVE_AREAS="${LB_ARCHIVE_AREAS:-$(Get_configuration config/build Archive-Areas)}"
	LB_ARCHIVE_AREAS="${LB_ARCHIVE_AREAS:-main}"
	export LB_ARCHIVE_AREAS

	# Image: Archive Areas
	LB_PARENT_ARCHIVE_AREAS="${LB_PARENT_ARCHIVE_AREAS:-$(Get_configuration config/build Parent-Archive-Areas)}"
	LB_PARENT_ARCHIVE_AREAS="${LB_PARENT_ARCHIVE_AREAS:-${LB_ARCHIVE_AREAS}}"
	export LB_PARENT_ARCHIVE_AREAS

	# Image: Type
	LIVE_IMAGE_TYPE="${LIVE_IMAGE_TYPE:-$(Get_configuration config/build Type)}"
	LIVE_IMAGE_TYPE="${LIVE_IMAGE_TYPE:-iso-hybrid}"
	export LIVE_IMAGE_TYPE
}

Set_config_defaults ()
{
	# FIXME
	New_configuration

	## config/common

	# Setting system type
	LB_SYSTEM="${LB_SYSTEM:-live}"

	# Setting mode (currently: debian)
	if [ $(which lsb_release) ]
	then
		local _DISTRIBUTOR
		_DISTRIBUTOR="$(lsb_release -is | tr "[A-Z]" "[a-z]")"

		case "${_DISTRIBUTOR}" in
			debian)
				LB_MODE="${LB_MODE:-${_DISTRIBUTOR}}"
				;;

			*)
				LB_MODE="${LB_MODE:-debian}"
				;;
		esac
	else
		LB_MODE="${LB_MODE:-debian}"
	fi

	# Setting distribution name
	LB_DERIVATIVE="false"
	LB_DISTRIBUTION="${LB_DISTRIBUTION:-buster}"
	LB_DISTRIBUTION_CHROOT="${LB_DISTRIBUTION_CHROOT:-${LB_DISTRIBUTION}}"
	LB_DISTRIBUTION_BINARY="${LB_DISTRIBUTION_BINARY:-${LB_DISTRIBUTION_CHROOT}}"

	LB_BACKPORTS="${LB_BACKPORTS:-false}"
	if [ -n "$LB_PARENT_DISTRIBUTION" ]; then
		LB_PARENT_DISTRIBUTION_CHROOT="${LB_PARENT_DISTRIBUTION_CHROOT:-${LB_PARENT_DISTRIBUTION}}"
		LB_PARENT_DISTRIBUTION_BINARY="${LB_PARENT_DISTRIBUTION_BINARY:-${LB_PARENT_DISTRIBUTION}}"
	else
		LB_PARENT_DISTRIBUTION_CHROOT="${LB_PARENT_DISTRIBUTION_CHROOT:-${LB_DISTRIBUTION_CHROOT}}"
		LB_PARENT_DISTRIBUTION_BINARY="${LB_PARENT_DISTRIBUTION_BINARY:-${LB_DISTRIBUTION_BINARY}}"
	fi
	LB_PARENT_DEBIAN_INSTALLER_DISTRIBUTION="${LB_PARENT_DEBIAN_INSTALLER_DISTRIBUTION:-${LB_PARENT_DISTRIBUTION_CHROOT}}"

	# Setting package manager
	LB_APT="${LB_APT:-apt}"

	# Setting apt ftp proxy
	LB_APT_FTP_PROXY="${LB_APT_FTP_PROXY}"

	# Setting apt http proxy
	LB_APT_HTTP_PROXY="${LB_APT_HTTP_PROXY}"

	# Setting apt pipeline
	# LB_APT_PIPELINE

	APT_OPTIONS="${APT_OPTIONS:---yes}"
	APTITUDE_OPTIONS="${APTITUDE_OPTIONS:---assume-yes}"

	BZIP2_OPTIONS="${BZIP2_OPTIONS:--6}"

	GZIP_OPTIONS="${GZIP_OPTIONS:--6}"

	if gzip --help | grep -qs "\-\-rsyncable"
	then
		GZIP_OPTIONS="$(echo ${GZIP_OPTIONS} | sed -e 's|--rsyncable||') --rsyncable"
	fi

	LZIP_OPTIONS="${LZIP_OPTIONS:--6}"

	LZMA_OPTIONS="${LZMA_OPTIONS:--6}"

	XZ_OPTIONS="${XZ_OPTIONS:--6}"

	# Setting apt settings
	LB_APT_RECOMMENDS="${LB_APT_RECOMMENDS:-true}"
	LB_APT_SECURE="${LB_APT_SECURE:-true}"
	LB_APT_SOURCE_ARCHIVES="${LB_APT_SOURCE_ARCHIVES:-true}"

	# Setting cache option
	LB_CACHE="${LB_CACHE:-true}"
	if [ "${LB_CACHE}" = "false" ]
	then
		LB_CACHE_INDICES="false"
		LB_CACHE_PACKAGES="false"
		LB_CACHE_STAGES="bootstrap" #bootstrap caching currently required for process to work
	else
		LB_CACHE_INDICES="${LB_CACHE_INDICES:-false}"
		LB_CACHE_PACKAGES="${LB_CACHE_PACKAGES:-true}"
		LB_CACHE_STAGES="${LB_CACHE_STAGES:-bootstrap}"
	fi

	# Setting debconf frontend
	LB_DEBCONF_FRONTEND="${LB_DEBCONF_FRONTEND:-noninteractive}"
	LB_DEBCONF_PRIORITY="${LB_DEBCONF_PRIORITY:-critical}"

	# Setting initramfs hook
	case "${LB_SYSTEM}" in
		live)
			LB_INITRAMFS="${LB_INITRAMFS:-live-boot}"
			;;

		normal)
			LB_INITRAMFS="${LB_INITRAMFS:-none}"
			;;
	esac

	LB_INITRAMFS_COMPRESSION="${LB_INITRAMFS_COMPRESSION:-gzip}"

	# Setting initsystem
	case "${LB_SYSTEM}" in
		live)
			LB_INITSYSTEM="${LB_INITSYSTEM:-systemd}"
			;;

		normal)
			LB_INITSYSTEM="${LB_INITSYSTEM:-none}"
			;;
	esac

	if [ "${LB_ARCHITECTURES}" = "i386" ] && [ "${CURRENT_IMAGE_ARCHITECTURE}" = "amd64" ]
	then
		# Use linux32 when building amd64 images on i386
		_LINUX32="linux32"
	else
		_LINUX32=""
	fi

	# Setting tasksel
	LB_TASKSEL="${LB_TASKSEL:-apt}"

	# Setting live build options
	# Colouring is re-evaluated here just incase a hard coded override was given in the saved config
	case "${_COLOR}" in
		true)
			_COLOR_OUT="true"
			_COLOR_ERR="true"
			;;
		false)
			_COLOR_OUT="false"
			_COLOR_ERR="false"
			;;
		auto)
			;;
	esac
	_BREAKPOINTS="${_BREAKPOINTS:-false}"
	_DEBUG="${_DEBUG:-false}"
	_FORCE="${_FORCE:-false}"
	_QUIET="${_QUIET:-false}"
	_VERBOSE="${_VERBOSE:-false}"

	# Apt v2.0.1 introduced color support, but it needs to be explicitly enabled
	if [ "${_COLOR_OUT}" = "true" ] && [ "${_COLOR_ERR}" = "true" ]; then
		APT_OPTIONS="${APT_OPTIONS} -o APT::Color=true"
		APTITUDE_OPTIONS="${APTITUDE_OPTIONS} -o APT::Color=true"
	else
		APT_OPTIONS="${APT_OPTIONS} -o APT::Color=false"
		APTITUDE_OPTIONS="${APTITUDE_OPTIONS} -o APT::Color=false"
	fi

	## config/bootstrap

	# Setting mirrors
	# *_MIRROR_CHROOT: to fetch packages from
	# *_MIRROR_BOOTSTRAP: to fetch packages from
	# *_MIRROR_CHROOT_SECURITY: security mirror to fetch packages from
	# *_MIRROR_BINARY: mirror which ends up in the image
	# *_MIRROR_BINARY_SECURITY: security mirror which ends up in the image
	# *_MIRROR_DEBIAN_INSTALLER: to fetch installer from
	LB_MIRROR_CHROOT="${LB_MIRROR_CHROOT:-${LB_MIRROR_BOOTSTRAP}}"
	LB_PARENT_MIRROR_CHROOT="${LB_PARENT_MIRROR_CHROOT:-${LB_PARENT_MIRROR_BOOTSTRAP}}"
	if [ "${LB_MODE}" = "debian" ]; then
		LB_MIRROR_BOOTSTRAP="${LB_MIRROR_BOOTSTRAP:-http://deb.debian.org/debian/}"
		LB_PARENT_MIRROR_BOOTSTRAP="${LB_PARENT_MIRROR_BOOTSTRAP:-${LB_MIRROR_BOOTSTRAP}}"

		LB_MIRROR_CHROOT_SECURITY="${LB_MIRROR_CHROOT_SECURITY:-http://security.debian.org/}"
		LB_PARENT_MIRROR_CHROOT_SECURITY="${LB_PARENT_MIRROR_CHROOT_SECURITY:-${LB_MIRROR_CHROOT_SECURITY}}"

		LB_MIRROR_BINARY="${LB_MIRROR_BINARY:-http://deb.debian.org/debian/}"
		LB_PARENT_MIRROR_BINARY="${LB_PARENT_MIRROR_BINARY:-${LB_MIRROR_BINARY}}"

		LB_MIRROR_BINARY_SECURITY="${LB_MIRROR_BINARY_SECURITY:-http://security.debian.org/}"
		LB_PARENT_MIRROR_BINARY_SECURITY="${LB_PARENT_MIRROR_BINARY_SECURITY:-${LB_MIRROR_BINARY_SECURITY}}"
	fi
	LB_MIRROR_DEBIAN_INSTALLER="${LB_MIRROR_DEBIAN_INSTALLER:-${LB_MIRROR_CHROOT}}"
	LB_PARENT_MIRROR_DEBIAN_INSTALLER="${LB_PARENT_MIRROR_DEBIAN_INSTALLER:-${LB_PARENT_MIRROR_CHROOT}}"

	## config/chroot

	# Setting chroot filesystem
	LB_CHROOT_FILESYSTEM="${LB_CHROOT_FILESYSTEM:-squashfs}"

	# Setting union filesystem
	LB_UNION_FILESYSTEM="${LB_UNION_FILESYSTEM:-overlay}"

	# Setting interactive shell/X11/Xnest
	LB_INTERACTIVE="${LB_INTERACTIVE:-false}"

	# Setting keyring packages
	LB_KEYRING_PACKAGES="${LB_KEYRING_PACKAGES:-debian-archive-keyring}"

	# Setting linux flavour string
	case "${LB_ARCHITECTURES}" in
		arm64)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-arm64}"
			;;

		armel)
			# armel will have special images: one rootfs image and many additional kernel images.
			# therefore we default to all available armel flavours
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-marvell}"
			;;

		armhf)
			# armhf will have special images: one rootfs image and many additional kernel images.
			# therefore we default to all available armhf flavours
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-armmp armmp-lpae}"
			;;

		amd64)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-amd64}"
			;;

		i386)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-686-pae}"
			;;

		ia64)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-itanium}"
			;;

		powerpc)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-powerpc64 powerpc}"
			;;

		s390x)
			LB_LINUX_FLAVOURS_WITH_ARCH="${LB_LINUX_FLAVOURS_WITH_ARCH:-s390x}"
			;;

		*)
			Echo_error "Architecture(s) ${LB_ARCHITECTURES} not yet supported (FIXME)"
			exit 1
			;;
	esac

	LB_LINUX_FLAVOURS=""
	for FLAVOUR in ${LB_LINUX_FLAVOURS_WITH_ARCH}
	do
		ARCH_FILTERED_FLAVOUR="$(echo ${FLAVOUR} | awk -F':' '{print $1}')"
		LB_LINUX_FLAVOURS="${LB_LINUX_FLAVOURS:+$LB_LINUX_FLAVOURS }${ARCH_FILTERED_FLAVOUR}"
	done


	# Set linux packages
	LB_LINUX_PACKAGES="${LB_LINUX_PACKAGES:-linux-image}"

	# Setting security updates option
	case "${LB_PARENT_DISTRIBUTION_BINARY}" in
		sid)
			LB_SECURITY="${LB_SECURITY:-false}"
			;;

		*)
			LB_SECURITY="${LB_SECURITY:-true}"
			;;
	esac

	# Setting updates updates option
	case "${LB_PARENT_DISTRIBUTION_BINARY}" in
		sid)
			LB_UPDATES="${LB_UPDATES:-false}"
			;;

		*)
			LB_UPDATES="${LB_UPDATES:-true}"
			;;
	esac

	## config/binary

	# Setting image filesystem
	LB_BINARY_FILESYSTEM="${LB_BINARY_FILESYSTEM:-fat32}"

	# Setting image type
	case "${LB_ARCHITECTURES}" in
		amd64|i386)
			LIVE_IMAGE_TYPE="${LIVE_IMAGE_TYPE:-iso-hybrid}"
			;;

		*)
			LIVE_IMAGE_TYPE="${LIVE_IMAGE_TYPE:-iso}"
			;;
	esac

	# Setting apt indices
	LB_APT_INDICES="${LB_APT_INDICES:-true}"

	# Setting bootloader
	if [ -z "${LB_BOOTLOADERS}" ]
	then
		case "${LB_ARCHITECTURES}" in
			amd64|i386)
				case "${LIVE_IMAGE_TYPE}" in
					hdd*|netboot)
						LB_BOOTLOADERS="syslinux"
						;;
					*)
						LB_BOOTLOADERS="syslinux,grub-efi"
						;;
				esac
				;;
		esac
	fi

	LB_FIRST_BOOTLOADER=$(echo "${LB_BOOTLOADERS}" | awk -F, '{ print $1 }')

	# Setting checksums
	LB_CHECKSUMS="${LB_CHECKSUMS:-sha256}"

	# Setting compression
	LB_COMPRESSION="${LB_COMPRESSION:-none}"

	# Setting zsync
	LB_ZSYNC="${LB_ZSYNC:-true}"

	# Setting chroot option
	LB_BUILD_WITH_CHROOT="${LB_BUILD_WITH_CHROOT:-true}"

	LB_BUILD_WITH_TMPFS="${LB_BUILD_WITH_TMPFS:-false}"

	# Setting debian-installer option
	LB_DEBIAN_INSTALLER="${LB_DEBIAN_INSTALLER:-none}"
	if [ "${LB_DEBIAN_INSTALLER}" = "false" ]
	then
		LB_DEBIAN_INSTALLER="none"
		Echo_warning "A value of 'false' for option LB_DEBIAN_INSTALLER is deprecated, please use 'none' in future."
	fi
	if [ "${LB_DEBIAN_INSTALLER}" = "true" ]
	then
		LB_DEBIAN_INSTALLER="netinst"
		Echo_warning "A value of 'true' for option LB_DEBIAN_INSTALLER is deprecated, please use 'netinst' in future."
	fi

	LB_DEBIAN_INSTALLER_DISTRIBUTION="${LB_DEBIAN_INSTALLER_DISTRIBUTION:-${LB_DISTRIBUTION}}"

	# Setting debian-installer-gui
	LB_DEBIAN_INSTALLER_GUI="${LB_DEBIAN_INSTALLER_GUI:-true}"

	# Setting debian-installer preseed filename
	if [ -z "${LB_DEBIAN_INSTALLER_PRESEEDFILE}" ]
	then
		if Find_files config/debian-installer/preseed.cfg
		then
			LB_DEBIAN_INSTALLER_PRESEEDFILE="/preseed.cfg"
		fi

		if Find_files config/debian-installer/*.cfg && [ ! -e config/debian-installer/preseed.cfg ]
		then
			Echo_warning "You have placed some preseeding files into config/debian-installer but you didn't specify the default preseeding file through LB_DEBIAN_INSTALLER_PRESEEDFILE. This means that debian-installer will not take up a preseeding file by default."
		fi
	fi

	# Setting boot parameters
	case "${LB_INITRAMFS}" in
		live-boot)
			LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE:-boot=live components quiet splash}"
			LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE:-boot=live components memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=788}"
			;;

		none)
			LB_BOOTAPPEND_LIVE="${LB_BOOTAPPEND_LIVE:-quiet splash}"
			LB_BOOTAPPEND_LIVE_FAILSAFE="${LB_BOOTAPPEND_LIVE_FAILSAFE:-memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=788}"
			;;
	esac

	local _LB_BOOTAPPEND_PRESEED
	if [ -n "${LB_DEBIAN_INSTALLER_PRESEEDFILE}" ]
	then
		case "${LIVE_IMAGE_TYPE}" in
			iso*)
				_LB_BOOTAPPEND_PRESEED="file=/cdrom/install/${LB_DEBIAN_INSTALLER_PRESEEDFILE}"
				;;

			hdd*)
				_LB_BOOTAPPEND_PRESEED="file=/hd-media/install/${LB_DEBIAN_INSTALLER_PRESEEDFILE}"
				;;

			netboot)
				case "${LB_DEBIAN_INSTALLER_PRESEEDFILE}" in
					*://*)
						_LB_BOOTAPPEND_PRESEED="file=${LB_DEBIAN_INSTALLER_PRESEEDFILE}"
						;;

					*)
						_LB_BOOTAPPEND_PRESEED="file=/${LB_DEBIAN_INSTALLER_PRESEEDFILE}"
						;;
				esac
				;;
		esac
	fi

	if [ -n ${_LB_BOOTAPPEND_PRESEED} ]
	then
		LB_BOOTAPPEND_INSTALL="${LB_BOOTAPPEND_INSTALL} ${_LB_BOOTAPPEND_PRESEED}"
	fi

	LB_BOOTAPPEND_INSTALL="$(echo ${LB_BOOTAPPEND_INSTALL} | sed -e 's/[ \t]*$//')"

	# Setting iso author
	LB_ISO_APPLICATION="${LB_ISO_APPLICATION:-Debian Live}"

	# Set iso preparer
	LB_ISO_PREPARER="${LB_ISO_PREPARER:-live-build \$VERSION; https://salsa.debian.org/live-team/live-build}"

	# Set iso publisher
	LB_ISO_PUBLISHER="${LB_ISO_PUBLISHER:-Debian Live project; https://wiki.debian.org/DebianLive; debian-live@lists.debian.org}"

	# Setting hdd options
	LB_HDD_LABEL="${LB_HDD_LABEL:-DEBIAN_LIVE}"

	# Setting hdd size
	LB_HDD_SIZE="${LB_HDD_SIZE:-auto}"

	# Setting iso volume
	LB_ISO_VOLUME="${LB_ISO_VOLUME:-Debian ${LB_DISTRIBUTION} \$(date +%Y%m%d-%H:%M)}"

	# Setting memtest option
	LB_MEMTEST="${LB_MEMTEST:-none}"
	if [ "${LB_MEMTEST}" = "false" ]; then
		LB_MEMTEST="none"
	fi

	# Setting loadlin option
	case "${LB_ARCHITECTURES}" in
		amd64|i386)
			if [ "${LB_DEBIAN_INSTALLER}" != "none" ]
			then
				LB_LOADLIN="${LB_LOADLIN:-true}"
			else
				LB_LOADLIN="${LB_LOADLIN:-false}"
			fi
			;;

		*)
			LB_LOADLIN="${LB_LOADLIN:-false}"
			;;
	esac

	# Setting win32-loader option
	case "${LB_ARCHITECTURES}" in
		amd64|i386)
			if [ "${LB_DEBIAN_INSTALLER}" != "none" ]
			then
				LB_WIN32_LOADER="${LB_WIN32_LOADER:-true}"
			else
				LB_WIN32_LOADER="${LB_WIN32_LOADER:-false}"
			fi
			;;

		*)
			LB_WIN32_LOADER="${LB_WIN32_LOADER:-false}"
			;;
	esac

	# Setting netboot filesystem
	LB_NET_ROOT_FILESYSTEM="${LB_NET_ROOT_FILESYSTEM:-nfs}"

	# Setting netboot server path
	LB_NET_ROOT_PATH="${LB_NET_ROOT_PATH:-/srv/${LB_MODE}-live}"

	# Setting netboot server address
	LB_NET_ROOT_SERVER="${LB_NET_ROOT_SERVER:-192.168.1.1}"

	# Setting net cow filesystem
	LB_NET_COW_FILESYSTEM="${LB_NET_COW_FILESYSTEM:-nfs}"

	# Setting net tarball
	LB_NET_TARBALL="${LB_NET_TARBALL:-true}"

	# Setting onie
	LB_ONIE="${LB_ONIE:-false}"

	# Setting onie additional kernel cmdline options
	LB_ONIE_KERNEL_CMDLINE="${LB_ONIE_KERNEL_CMDLINE:-}"

	# Setting firmware option
	LB_FIRMWARE_CHROOT="${LB_FIRMWARE_CHROOT:-true}"
	LB_FIRMWARE_BINARY="${LB_FIRMWARE_BINARY:-true}"

	# Setting swap file
	LB_SWAP_FILE_SIZE="${LB_SWAP_FILE_SIZE:-512}"

	# Setting UEFI Secure Boot
	LB_UEFI_SECURE_BOOT="${LB_UEFI_SECURE_BOOT:-auto}"

	## config/source

	# Setting source option
	LB_SOURCE="${LB_SOURCE:-false}"

	# Setting image type
	LB_SOURCE_IMAGES="${LB_SOURCE_IMAGES:-tar}"
}

Check_config_defaults ()
{
	case "${LB_BINARY_FILESYSTEM}" in
		ntfs)
			if [ ! $(which ntfs-3g) ]
			then
				Echo_error "Using ntfs as the binary filesystem is currently only supported if ntfs-3g is installed on the host system."

				exit 1
			fi
			;;
	esac

	if echo ${LB_HDD_LABEL} | grep -qs ' '
	then
		Echo_error "There are currently no whitespaces supported in hdd labels."

		exit 1
	fi

	if [ "${LB_DEBIAN_INSTALLER}" != "none" ]
	then
		# d-i true, no caching
		if ! echo ${LB_CACHE_STAGES} | grep -qs "bootstrap\b" || [ "${LB_CACHE}" != "true" ] || [ "${LB_CACHE_PACKAGES}" != "true" ]
		then
			Echo_warning "You have selected values of LB_CACHE, LB_CACHE_PACKAGES, LB_CACHE_STAGES and LB_DEBIAN_INSTALLER which will result in 'bootstrap' packages not being cached. This configuration is potentially unsafe as the bootstrap packages are re-used when integrating the Debian Installer."
		fi
	fi

	if [ "${LB_FIRST_BOOTLOADER}" = "syslinux" ]
	then
		# syslinux + fat or ntfs, or extlinux + ext[234] or btrfs
		case "${LB_BINARY_FILESYSTEM}" in
			fat*|ntfs|ext[234]|btrfs)
				;;
			*)
				Echo_warning "You have selected values of LB_BOOTLOADERS and LB_BINARY_FILESYSTEM which are incompatible - the syslinux family only support FAT, NTFS, ext[234] or btrfs filesystems."
				;;
		esac
	fi

	case "${LIVE_IMAGE_TYPE}" in
		hdd*)
			case "${LB_FIRST_BOOTLOADER}" in
				grub)
					Echo_error "You have selected a combination of bootloader and image type that is currently not supported by live-build. Please use either another bootloader or a different image type."
					exit 1
					;;
			esac
			;;
	esac

	if [ "$(echo \"${LB_ISO_APPLICATION}\" | wc -c)" -gt 128 ]
	then
		Echo_warning "You have specified a value of LB_ISO_APPLICATION that is too long; the maximum length is 128 characters."
	fi

	if [ "$(echo \"${LB_ISO_PREPARER}\" | wc -c)" -gt  128 ]
	then
		Echo_warning "You have specified a value of LB_ISO_PREPARER that is too long; the maximum length is 128 characters."
	fi

	if [ "$(echo \"${LB_ISO_PUBLISHER}\" | wc -c)" -gt 128 ]
	then
		Echo_warning "You have specified a value of LB_ISO_PUBLISHER that is too long; the maximum length is 128 characters."
	fi

	if [ "$(eval "echo \"${LB_ISO_VOLUME}\"" | wc -c)" -gt 32 ]
	then
		Echo_warning "You have specified a value of LB_ISO_VOLUME that is too long; the maximum length is 32 characters."
	fi

	# Architectures to use foreign bootstrap for
	LB_BOOTSTRAP_QEMU_ARCHITECTURES="${LB_BOOTSTRAP_QEMU_ARCHITECTURES:-}"

	# Packages to exclude for the foreign/ports bootstrapping
	LB_BOOTSTRAP_QEMU_EXCLUDE="${LB_BOOTSTRAP_QEMU_EXCLUDE:-}"

	# Ports using foreign bootstrap need a working qemu-*-system. This is the location it
	LB_BOOTSTRAP_QEMU_STATIC="${LB_BOOTSTRAP_QEMU_STATIC:-}"

}
